package com.bitbucket.http;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(8080);

        System.out.println("Server started with port " + 8080);

        while (true) {

            Socket socket = serverSocket.accept();
            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));

            String str;
            int contLength = -1;

            while ((str = br.readLine()) != null && !str.isEmpty()) {
                System.out.println(str);
                if(str.contains("Content-Length:")){
                    contLength = Integer.parseInt(str.substring("Content-Length: ".length()));
                }
            }


            if(contLength != -1){
                char[] body = new char[contLength];
                br.read(body,0,contLength);
                System.out.println(body);
            }

            System.out.println(contLength);
            String outString = "Olololo";

            int length = outString.length();

            bw.write("HTTP/1.1 200 OK\n\r" +
                    "Content-Length: " + length + "\n\r" +
                    "Content-Type: text/plain\n\r"
            );

            bw.write("\n" + outString + "1234\n\r\n\r");

            System.out.println();

            System.out.println("Socket closed.");

            bw.flush();
            bw.close();
            br.close();
            socket.close();

        }


    }

}